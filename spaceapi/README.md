Python scripts working with MQTT
===================

Resolver
============
Accepts list of MAC addresses, outputs list of active users

Spaceapi
============
Accepts list of active users, updates file reporting Hacklab status 


Configuration
=============

Currently config is stored in YAML file. Example of config:

```
ftp:
  host: host.ftp.tools
  user: user_ftp
  password: pass
mqtt:
  host: m10.cloudmqtt.com
  port: 16860
  user: user
  password: pass
  macs-topic: presence/macs
  users-topic: presence/users
website:
  folder: /example.com/www/files
logging: spaceapi.log
data: spaceapi.json
db: 
  user: user
  password: pass
  host: localhost
  port: 5432
  name: name
  
```

By default, config file name is `config.cfg`

