Flashing readers
================

NodeMCU firmware
----------------

Either build one (TODO: instructions) or take a pre-built one (0x00000.bin and 0x10000.bin in this directory).

Firmware can be integer-only, but must include uart, bit, tmr (TODO: full list) modules.

Flash NodeMCU firmware
----------------------

1. Fetch the latest version of https://github.com/espressif/esptool
2. Connect serial port (TODO: photo)
3. Put the flash jumper (TODO: photo)
4. $ ./esptool.py --port /dev/ttyUSB0 write_flash -fm dio 0x00000 ../main/lua/0x00000.bin 0x10000 ../main/lua/0x10000.bin
 - If it can't connect it is worth trying to short the reset pin (TODO: photo) while it's trying to connect
 - If it doesn't help the problem in 99% cases was soldering of the PCB. Good luck.
5. Remove the flash jumper
6. Restart the board

Initialize WiFi
---------------

0. Fetch the latest version of https://github.com/kmpm/nodemcu-uploader
1. Edit ../main/lua/wifi-init.lua, insert the desired ssid/password at the top
2. $ ./nodemcu-uploader.py --port /dev/ttyUSB0 upload ../main/lua/wifi-init.lua:wifi.lua
3. $ ./nodemcu-uploader.py --port /dev/ttyUSB0 file list
 - Just to verify
4. $ ./nodemcu-uploader.py --port /dev/ttyUSB0 file do wifi.lua && picocom --baud 115200 /dev/ttyUSB0
 - picocom command just to see the logs
 - wifi settings are stored, so this step needs to be performed just once per firmware flash

This step usually works flawlessly, given that firmware was flashed successfully.

Test reader
-----------

0. Edit ../main/lua/rfid.lua, insert the desired port at the top, replace the second parameter to uart.on to '3' (just for convenience!)
1. $ ./nodemcu-uploader.py --port /dev/ttyUSB0 upload ../main/lua/rfid.lua:rfid.lua
2. $ ./nodemcu-uploader.py --port /dev/ttyUSB0 file list
 - Just to verify
3. $ ./nodemcu-uploader.py --port /dev/ttyUSB0 file do rfid.lua
 - After a short time this command will print some failures, the reason being that rfid.lua hijacks the UART
4. picocom --baud 9600 /dev/ttyUSB0
 - We now emulate the rfid reader board (which is not connected to ESP8266 at the moment)
5. Input exactly 13 characters not equal to '3', and then '3'
 - This emulates key reading
 - Check that backend receives the request
 - It is possible to add debug statements to the rfid.lua with uart.write(0, "...something...")
 - It is also possible to test relay switch by modifying the logic in rfid.lua (or backend, good luck) - but relay is separate from rfid board/MCU

If everything works we are ready to productionize the reader!

Productionizing the reader
--------------------------

1. $ ./nodemcu-uploader.py --port /dev/ttyUSB0 upload ../main/lua/init.lua:init.lua
2. Remove usb/uart connection and put on two jumpers (TODO: photo)
3. Put on the rfid board with antenna
4. Write port number from rfid.lua on the back of the board with a marker
5. Restart the board
6. TODO: explanation of why we would need to re-flash it now

Congratulations!

Tips & Tricks
=============

TODO:
- Testing the rfid board
- Sniffing uart communication between the rfid board and the mcu
- Resetting the board
- RFID board *needs* 5V
- Relay works with 3.3V
- If resetting doesn't blink the blue diod your board might be dead
- Check relay by shorting control pin to either VCC or ground
