local tm1637 = require('tm1637')
local tm1637a = require('tm1637a')

-- Config
local MISO = 5            --> GPIO14
local CLK = 6             --> GPIO12
local CS = 7              --> GPIO13
local duration = 3000     --> 3 seconds

local SEGMENT_CLK = 4
local SEGMENT_DIO = 3
local SEGMENT_DIO2 = 10
local SEGMENT_CLK2 = 9

local RELAY_PIN = 2

local PREVN = 3
local KP = 2122
-- Because we use (result - prev2) that is for 2 steps
local KD = 25358 / PREVN
local KI = 44

local MAXOUT = 5000

-- Variables
local relayState = 0
local mqttClient = mqtt.Client("esp8266-thermo", 120)
local temperatureTimer = tmr.create()
local setLevel = 0
local intterm = 0
local prev = {}


function setup()
  tm1637.init(SEGMENT_CLK, SEGMENT_DIO)
  tm1637.clear()
  tm1637.set_brightness(7)
  tm1637a.init(SEGMENT_CLK2, SEGMENT_DIO2)
  tm1637a.clear()
  tm1637a.set_brightness(7)

  -- Pin Initialization
  gpio.mode(CS, gpio.OUTPUT)
  gpio.mode(CLK, gpio.OUTPUT)
  gpio.mode(MISO, gpio.INPUT)
  gpio.write(CS, gpio.HIGH)

  gpio.mode(RELAY_PIN, gpio.OUTPUT)

  -- Create an interval
  temperatureTimer:alarm(duration, tmr.ALARM_AUTO, readSPI)
  while #prev < PREVN do
    table.insert(prev, 0)
  end
end

function finalize()
    local result
    print("finalizing")
    result = mqttClient:close()
    print(result)
    temperatureTimer:unregister()
    print("unregistered")
end

function readSPI()
  gpio.write(CS, gpio.LOW)      -->Activate the chip
  tmr.delay(100)                -->100us Delay

  local result = 0

  for i = 1, 16 do
    gpio.write(CLK, gpio.LOW)
    tmr.delay(100)
    result = bit.lshift(result, 1)
    local input = gpio.read(MISO)
    if input ~= 0 then
      result = bit.set(result, 0)
    end
    gpio.write(CLK, gpio.HIGH)
    tmr.delay(100)
  end

  gpio.write(CS, gpio.HIGH)

  if bit.isset(result, 2) then                      -- refer MAX6675 Datasheet
    print("Sensor not connected", result)
    mqttClient:publish("/thermo/stats", "-1", 0, 0)
    -- tm1637.write_string('0000')
    return
  end

  result = bit.rshift(result, 5)
  print(result)
  tm1637.write_string(tostring(result))
  tm1637a.write_string(tostring(setLevel))
  mqttClient:publish("/thermo/stats", result, 0, 0)

  -- controller
  --[[
  -- bang-bang
  if result < setLevel and setLevel > 0 then
    relayState = 1
  elseif result > setLevel or setLevel == 0 then
    relayState = 0
  end
  ]]--

  -- PID
  -- Pu: 101 -> 579 in 5 intervals => 95.6
  -- A: (111+114+111+114+112)/5 - (95+95+94+94+94)/5 = 112.4-94.4 = 18
  -- D: 0.5
  -- Ku = (4 * D) / (A * Pi) = 0.0353677651315323
  -- Kp = 0.6 * Ku = 0.021220659
  -- Ki = 1.2 * Ku / Pu = 0.000443946842655
  -- Kd = 0.075 * Ku * Pu = 0.253586876
  --  2.1220659 = 2122
  --  0.0443947 = 44
  -- 25.3586876 = 25358

  local err = setLevel - result
  intterm = intterm + KI * err
  if intterm > MAXOUT then
    intterm = MAXOUT
  elseif intterm < -MAXOUT then
    intterm = -MAXOUT
  end
  local difterm = result - prev[1]
  local out = KP * err + intterm - KD * difterm
  print('DEBUG', #prev, setLevel, result, err, difterm, KP * err, intterm, KD * difterm, out)
  if out > 0 then
    relayState = 1
  else
    relayState = 0
  end

  table.remove(prev, 1)
  table.insert(prev, result)

  gpio.write(RELAY_PIN, relayState)
end

mqttClient:on("connect", function(client) print ("connected") end)
--mqttClient:on("connfail", function(client, reason) print ("connection failed", reason) end)
mqttClient:on("offline", function(client) print ("offline") end)
mqttClient:on("overflow", function(client, topic, data)
  print(topic .. " partial overflowed message: " .. data )
end)

mqttClient:on("message", function(client, topic, data)
  if topic == "/thermo/meta" then
    if data == "quit" then
       print("Quitting...")
       finalize()
    else 
      setLevel = tonumber(data)
      print("setLevel", setLevel)
    end
  end
end)

mqttClient:connect("test.mosquitto.org", 1883, false, function(client)
  print("connected")
  -- Calling subscribe/publish only makes sense once the connection
  -- was successfully established. You can do that either here in the
  -- 'connect' callback or you need to otherwise make sure the
  -- connection was established (e.g. tracking connection status or in
  -- mqttClient:on("connect", function)).
  client:subscribe("/thermo/meta", 0, function(client) print("subscribe success") end)
  -- publish a message with data = hello, QoS = 0, retain = 0
  client:publish("/thermo/stats", "hello", 0, 0, function(client) print("sent") end)
  setup()
end,
function(client, reason)
  print("failed reason: " .. reason)
end)
