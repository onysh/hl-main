import yaml
import json
import urllib.request
import datetime
import csv

CONFIG_FILE = 'config.cfg'
cfg = yaml.load(open(CONFIG_FILE, 'r'))
PRIVAT_ID = cfg['privat']['id']
PRIVAT_TOKEN = cfg['privat']['token']

#url = 'https://acp.privatbank.ua/api/proxy/date/'
headers = {
  'User-Agent' : 'Python3 baby',
  'Content-Type' : 'application/json;charset=utf8',
  'id' : PRIVAT_ID,
  'token' : PRIVAT_TOKEN }
#req = urllib.request.Request(url, None, headers)
#response = urllib.request.urlopen(req)
#beginning = datetime.datetime.strptime(json.loads(response.read())['today'], '%d.%m.%Y 00:00:00').date().replace(day=1)
beginning = datetime.datetime.today().replace(day=1)

for i in range(1):
  ending = beginning - datetime.timedelta(days=1)
  beginning = ending.replace(day=1)
  name = 'data/' + str(beginning.month) + '_' + str(beginning.year)  + '.csv'

  FORMAT = '%d-%m-%Y'
  url = 'https://acp.privatbank.ua/api/statements/transactions?startDate={}&endDate={}'.format(beginning.strftime(FORMAT), ending.strftime(FORMAT))
  print(url, '->', name)
  req = urllib.request.Request(url, None, headers)
  response = urllib.request.urlopen(req)
  result = json.loads(response.read())
  transactions = result['transactions']

  while result['exist_next_page']:
    url = 'https://acp.privatbank.ua/api/statements/transactions?startDate={}&endDate={}&followId={}'.format(beginning.strftime(FORMAT), ending.strftime(FORMAT), result['next_page_id'])
    req = urllib.request.Request(url, None, headers)
    response = urllib.request.urlopen(req)
    result = json.loads(response.read())
    transactions += result['transactions']

  with open(name, 'w') as out:
    csvout = csv.writer(out)
    for transaction in transactions:
      csvout.writerow([
          transaction['DAT_OD'],
          transaction['TRANTYPE'],
          transaction['SUM'],
          transaction['OSND'],
          transaction['AUT_CNTR_ACC'],
          transaction['AUT_CNTR_NAM']])
