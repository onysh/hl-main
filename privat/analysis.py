import csv
import datetime

beginning = datetime.datetime.today()
mnames = set([name[:-1] for name in open('data/names.txt')])

# Generating names
#names = open('data/names.txt', 'a')
#for i in range(12):
#  beginning = beginning.replace(day=1) - datetime.timedelta(days=1)
#  beginning = beginning.replace(day=1)
#  fname = 'data/month' + str(beginning.month) + '.csv'
#  with open(fname) as infile:
#    for row in csv.reader(infile):
#      if row[1] == 'C':
#        words = row[3].split()
#        explained = False
#        if not explained:
#          for mname in mnames:
#            last, first, patr = mname.split()
#            if first in row[3].lower() and last in row[3].lower():
#              explained = True
#            elif (row[4] != '29024866200031' and
#                  first in row[5].lower() and
#                  last in row[5].lower()):
#              explained = True
#        if not explained and len(words) >= 6:
#          if (words[-1].lower() == words[-4][:-1].lower() and
#              words[-2].lower() == words[-5].lower() and
#              words[-3].lower() == words[-6].lower()):
#            explained = True
#            name = ' '.join(map(lambda s: s.lower(), words[-3:]))
#            if name not in mnames:
#              names.write(name)
#              names.write('\n')
#              mnames.add(name)
#        if not explained:
#          print(words, row[4], row[5])

spent = {}
for i in range(12):
  beginning = beginning.replace(day=1) - datetime.timedelta(days=1)
  beginning = beginning.replace(day=1)
  fname = 'data/' + str(beginning.month) + '_' + str(beginning.year) + '.csv'
  with open(fname) as infile:
    for row in csv.reader(infile):
      if row[1] == 'C':
        for mname in mnames:
          triplets = mname.split()
          explained = False
          for (last, first, patr) in zip(triplets[::3], triplets[1::3], triplets[2::3]):
            if ((first in row[3].lower() and
                 last in row[3].lower()) or
                (first in row[5].lower() and
                 last in row[5].lower()) or
                (last in row[3].lower() and (first[0] + '.' + patr[0] + '.') in row[3].lower())):
              spent.setdefault(mname, [0.0]*12)[i] += float(row[2])
              explained = True
              break
          if explained:
            break
        else:
          print('Unexplained', row)

with open('data/pivot.csv', 'w') as outfile:
  outcsv = csv.writer(outfile)
  for name, vals in spent.items():
    outcsv.writerow([name] + vals)
  
