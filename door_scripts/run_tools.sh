#!/bin/sh


if pgrep -f "daemon_tools.py $1" >/dev/null; then
    echo "Process already running"
    exit 1
fi

python /root/daemon_tools.py $1 &>/dev/null &

exit 0 