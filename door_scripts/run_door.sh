#!/bin/sh


if pgrep -f "daemon_door.py" >/dev/null; then
    echo "Process already running"
    exit 1
fi

python /root/daemon_door.py &>/dev/null &

exit 0 