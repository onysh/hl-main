CREATE TABLE users ( id serial primary key, name text, key text, door integer, cnc integer, lathe integer, bigcnc integer, laser integer, bandsaw integer, mill integer );

CREATE TABLE macs ( id serial primary key, mac text, user_id integer references users(id) );
